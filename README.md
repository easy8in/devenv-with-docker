# devenv-with-docker

#### 介绍
在虚拟机快速搭建开发环境中需要的mysql和redis,目的是为了快速简单的提供开发环境.

#### 软件架构
采用docker＋docker-compose 快速启动

#### 安装教程(Centos&ubuntu)

1.  安装docker&docker-compose(**必须的**)
```shell
# 安装docker
curl -o- https://get.docker.com | bash
# 开机启动docker
systemctl enable docker
# 启动docker
systemctl start docker
# centos关闭防火墙
systemctl stop firewalld
# centos关闭防火墙开机启动
systemctl disable firewalld
# ubuntu 关闭防火墙
#systemctl stop ufw
#systemctl disable ufw

# 安装docker-compose
curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
# github网络慢,可以下面加速地址
#curl -L https://hub.fastgit.org/docker/compose/releases/download/1.29.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```
2.  克隆仓库
```bash
git clone https://gitee.com/easy8in/devenv-with-docker.git
```
3. 启动mysql&redis

```bash
# 启动mysql:5.7.34 用户名:root 密码:asdqwe123
# 修改root密码,在启动之前vim docker-compose.yml文件中对应的密码
cd mysql57
docker-compose up -d

# 启动redis:latest 验证码:asdqwe123
# 修改验证码,在启动之前vim docker-compose.yml 文件中的密码
cd redis
docker-compose up -d

# 验证结果 可以看到2个容器已经启动
docker ps
```

#### 注意项

1.  mysql在window中安装默认是忽略表名大小写的,所以在docker中为了兼容在window中的配置,当前启动的linux 容器也是忽略表名大小写的,不忽略的配置删除docker-compose.yml中的command中的--lower_case_table_names=1,volumes中的 ./conf/my.cnf:/etc/mysql/my.cnf
2.  mysql忽略表名大小写的配置,只能在容器第一次启动的时候才能生效
3.  redis配置的采用最新的redis容器
4.  生产环境不适合这么搞!
5.  生产环境不适合这么搞!
6.  生产环境不适合这么搞!

