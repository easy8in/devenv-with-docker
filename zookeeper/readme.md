```bash
cd zookeeper

mkdir -p zk1/data && mkdir -p zk1/log
mkdir -p zk2/data && mkdir -p zk2/log
mkdir -p zk3/data && mkdir -p zk3/log

docker-compose up -d

docker-compose ps

# 客户端端口
## zk1:2181 
## zk2:2182
## zk3:2183

# http查看
## http://${host}:8081/commands
## http://${host}:8082/commands
## http://${host}:8083/commands
```

